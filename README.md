# Drupal project manual tasks pipeline template 

## Description
This repository is a base template to implement Drupal project manual tasks pipelines only triggered by Gitlab WEB UI (Button "Run new pipeline"). 

The template offers stages to export, sync database, and sync, export files for exemple. It is up to you to extends those existing jobs to fit your project and needs. 

## Usage
In your .gitlabci.yml file add this template by using :

```
include:
- 'https://gitlab.com/grumpyDev74/drupal-project-manual-tasks-pipeline-template/raw/main/.gitlab-ci.yml'
 ```

<div align="center">

![Gitlab CI/CD logo](https://chezsoi.org/lucas/blog/images/2021/07/gitlab-ci.png)
<br>
![Drupal logo](https://www.drupal.org/files/Wordmark2_blue_RGB%281%29.png)

</div>
